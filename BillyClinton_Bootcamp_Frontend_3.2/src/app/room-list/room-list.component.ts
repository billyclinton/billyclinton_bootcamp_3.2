import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-room-list',
  templateUrl: './room-list.component.html',
  styleUrls: ['./room-list.component.css']
})
export class RoomListComponent implements OnInit {

  constructor() { }
  rooms:Object[]=[
    {"number":"1", "floor":"1", "type":"studio", "status":"Available"},
    {"number":"2", "floor":"1", "type":"studio", "status":"Unavailable"},
    {"number":"3", "floor":"1", "type":"studio", "status":"Available"},
    {"number":"4", "floor":"1", "type":"studio", "status":"Available"},
    {"number":"5", "floor":"1", "type":"studio", "status":"Unavailable"}
  ]
  ngOnInit() {
  }

}
