import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RoomListComponent } from './room-list/room-list.component';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { BookedListComponent } from './booked-list/booked-list.component';

@NgModule({
  declarations: [
    AppComponent,
    RoomListComponent,
    RoomListComponent,
    CustomerListComponent,
    BookedListComponent
   
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
