import { LoginProjectPage } from './app.po';

describe('login-project App', function() {
  let page: LoginProjectPage;

  beforeEach(() => {
    page = new LoginProjectPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
