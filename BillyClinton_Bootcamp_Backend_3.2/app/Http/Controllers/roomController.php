<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\transaction;
use App\room;

class roomController extends Controller
{
     function addData(request $request)
    {
        DB::beginTransaction();
        try{
            $order= new transaction;
            $order->room_id = $request->input('room_id');
            $order->customer_id= $request->input('customer_id');
            $order->check_in= $request->input('check_in');
            $order->check_out= $request->input('check_out');
            $order->save();
            $room = room::get()->where('room_id','=',$request->input('room_id'))->first();
            $room->status = "unavailable";
            $room->save();
            DB::commit();
            return response()->json(["message" => "success!!"], 200);
        }
        catch(\Exception $e){
            DB::rollBack();
            return response()->json(["message" => $e->getMessage()], 500);
        }
    }
}
